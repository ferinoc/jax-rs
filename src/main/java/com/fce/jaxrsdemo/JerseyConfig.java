package com.fce.jaxrsdemo;

import com.fce.jaxrsdemo.controller.AnimalBoxController;
import com.fce.jaxrsdemo.controller.MonkeyController;
import io.swagger.v3.jaxrs2.SwaggerSerializers;
import io.swagger.v3.jaxrs2.integration.JaxrsOpenApiContextBuilder;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.integration.OpenApiConfigurationException;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationPath("/api")
@Component
public class JerseyConfig extends ResourceConfig {

    @Value("${spring.jersey.application-path:/}")
    private String apiPath;


    @PostConstruct
    public void init() {
        // Register components where DI is needed
        this.configureSwagger();
    }

    private void configureSwagger() {
        // Available at localhost:port/api/swagger.json
//        this.register(ApiListingResource.class);
        this.register(SwaggerSerializers.class);

        this.register(MonkeyController.class);
        this.register(OpenApiResource.class);
        this.register(AnimalBoxController.class);

//        BeanConfig config = new BeanConfig();
//        config.setConfigId("springboot-jersey-swagger-docker-example");
//        config.setTitle("Spring Boot + Jersey + Swagger + Docker Example");
//        config.setVersion("v1");
//        config.setContact("Orlando L Otero");
//        config.setSchemes(new String[]{"http", "https"});
//        config.setBasePath(this.apiPath);
//        config.setResourcePackage("com.asimio.jerseyexample.rest.v1");
//        config.setPrettyPrint(true);
//        config.setScan(true);

        OpenAPI oas = new OpenAPI();
        Info info = new Info()
                .title("Swagger Sample App FCE")
                .description("FCE This is a sample server Petstore server.  You can find out more about Swagger " +
                        "at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).  For this sample, " +
                        "you can use the api key `special-key` to test the authorization filters.")
                .termsOfService("FCE http://swagger.io/terms/")
                .contact(new Contact()
                        .email("FCE apiteam@swagger.io"))
                .license(new License()
                        .name("FCE Apache 2.0")
                        .url("FCE http://www.apache.org/licenses/LICENSE-2.0.html"));

        oas.info(info);

        SwaggerConfiguration oasConfig = new SwaggerConfiguration()
                .openAPI(oas)
                .resourcePackages(Stream.of("io.swagger.sample.resource").collect(Collectors.toSet()));


        try {
            new JaxrsOpenApiContextBuilder()
//                    .servletConfig(config)
                    .openApiConfiguration(oasConfig)
                    .buildContext(true);
        } catch (OpenApiConfigurationException e) {
            e.printStackTrace();
        }

    }
}
