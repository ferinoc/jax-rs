package com.fce.jaxrsdemo.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.v3.oas.annotations.media.Schema;


//@JsonTypeInfo(
//        use = JsonTypeInfo.Id.NAME,
//        property = "type")
//@JsonSubTypes({
//        @JsonSubTypes.Type(value = MonkeyDto.class, name = "MonkeyDto"),
//        @JsonSubTypes.Type(value = ElephantDto.class, name = "ElephantDto")
//})
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = MonkeyDto.class),
        @JsonSubTypes.Type(value = ElephantDto.class)
})
@Schema(
        description = "animal dto",
        discriminatorProperty = "type",
//        subTypes = {ElephantDto.class, MonkeyDto.class},
        anyOf = {ElephantDto.class, MonkeyDto.class}
)
public class AnimalDto implements DTO {

//    private String type;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }

}
