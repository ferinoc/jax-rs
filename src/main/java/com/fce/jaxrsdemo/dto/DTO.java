package com.fce.jaxrsdemo.dto;

/**
 * Marker interface for DTOs.
 *
 * @author Daniel Westhoff
 * @since 1.1.0
 */
public interface DTO {

}
