package com.fce.jaxrsdemo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;

//@Schema(description = "elephant dto", discriminatorProperty = "discriminator", )

@Schema(
        description = "Elephant Dto",
        discriminatorProperty = "type",
//        subTypes = {ElephantDto.class, MonkeyDto.class},
        allOf = {AnimalDto.class})
@Value
@Builder(toBuilder = true)
public class ElephantDto extends AnimalDto {

    private final boolean hungry;

}
