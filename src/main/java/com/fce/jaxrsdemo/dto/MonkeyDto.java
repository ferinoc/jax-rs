package com.fce.jaxrsdemo.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(
        description = "Monkey Dto",
        discriminatorProperty = "type",
//        subTypes = {ElephantDto.class, MonkeyDto.class},
        allOf = {AnimalDto.class})
public class MonkeyDto extends AnimalDto {

    private boolean friendly;

    public boolean isFriendly() {
        return friendly;
    }

    public void setFriendly(boolean friendly) {
        this.friendly = friendly;
    }
}
