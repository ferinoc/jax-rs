package com.fce.jaxrsdemo.dto;

public class AnimalBox {

    private String id;

    private AnimalDto animal;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AnimalDto getAnimal() {
        return animal;
    }

    public void setAnimal(AnimalDto animal) {
        this.animal = animal;
    }
}
