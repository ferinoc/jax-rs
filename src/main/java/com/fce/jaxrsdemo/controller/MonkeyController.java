package com.fce.jaxrsdemo.controller;

import com.fce.jaxrsdemo.dto.AnimalDto;
import com.fce.jaxrsdemo.dto.MonkeyDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Component
@Path(value = "monkey")
@Tag(name = "monkey")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MonkeyController {

    @Operation(summary = "Returns monkey",
//            tags = {"monkey"},
            description = "Monkey description",
            responses = {
                    @ApiResponse(description = "The pet",
                            content = @Content(
                                    schema = @Schema(implementation = AnimalDto.class)
                            )),
                    @ApiResponse(responseCode = "400", description = "Invalid ID supplied"),
                    @ApiResponse(responseCode = "404", description = "Monkey not found")
            })
    @GET
    public Response getMonkey() {
        MonkeyDto monkey = new MonkeyDto();
        monkey.setName("monkey");
        monkey.setFriendly(true);

        AnimalDto animal = monkey;
        return Response.ok(animal).build();
    }

    @POST
    public Response createMonkey(MonkeyDto monkeyDto) {
        return Response.ok(monkeyDto).build();
    }

}
