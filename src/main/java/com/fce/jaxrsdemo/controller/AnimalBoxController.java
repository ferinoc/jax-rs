package com.fce.jaxrsdemo.controller;

import com.fce.jaxrsdemo.dto.AnimalBox;
import com.fce.jaxrsdemo.dto.MonkeyDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Component
@Path(value = "box")
@Tag(name = "box")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AnimalBoxController {

    @Operation(summary = "Returns one animal box",
//            tags = {"box"},
            description = "Returns one animal box, this is description",
            responses = {
                    @ApiResponse(description = "Return animal box",
                            content = @Content(
                                    schema = @Schema(implementation = AnimalBox.class)
                            )),
                    @ApiResponse(responseCode = "400", description = "Invalid ID supplied"),
                    @ApiResponse(responseCode = "404", description = "Pet not found")
            })
    @GET
    public Response getBox() {
        MonkeyDto monkey = new MonkeyDto();
        monkey.setName("monkey");
        monkey.setFriendly(true);

        AnimalBox box = new AnimalBox();
        box.setId("2019-01");
        box.setAnimal(monkey);
        return Response.ok(box).build();
    }

    @POST
    public Response createBox(AnimalBox box) {
        return Response.accepted(box).build();
    }

}
